
# :frog: RECAST :frog:

More info in the whole RECAST project can be found
https://recast-docs.web.cern.ch/recast-docs/#tldr

This example is a simple way on how to run RECAST for your HistFitter code.
The example shown here is the ones used for the higgsinos with soft leptons HL-LHC prospects ATL-PHYS-PUB-2018-031 or arXiv:1812.07831.

## Quick description of the files:

*  specs/steps.yml : this is where you specify the steps of the analysis, i.e., the command lines you normally run in the terminal.
*  specs/workflow.yml : this should be your workflow (e.g. event selection + statistical analysis), but here it's a dummy file since we just want to run HistFitter.
*  recast.yml : here you specify the parameters that are going to be used by steps.yml.

## Preparation
:warning: WARNING: this is intended to be run locally in your laptop (not e.g. in lxplus) :warning:

(Read [preparation.txt](./preparation.txt) for details)
*  Install docker from https://docs.docker.com/install/
*  Install pip
*  Install virtualenv and recast-atlas
```
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install recast-atlas
```
*  Create a personal access token on github : https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html


### At the beginning of each session you should
```
virtualenv venv
source venv/bin/activate
pip install recast-atlas==0.0.20
```


### To run

:warning: This example uses files under `/eos/user/s/susyupg/` , to access these contact <jorge.sabater.iglesias@desy.de> :warning:

```
git clone https://gitlab.cern.ch/jsabater/higgsino-upgrade.git
# If you are going to use files in eos space (which is the case in the example), you will need to setup the following
RECAST_USER=myUser # eos user (e.g. susyupg)
RECAST_PASS=myPass # password
RECAST_TOKEN=myToken # git token
eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"


# add the project
$(recast catalogue add higgsino-upgrade-hf)
# check that it was added
recast catalogue ls
# check that all is ok
recast catalogue check examples/higgsinos-hf
# check the steps were added
recast tests ls examples/higgsinos-hf
# to run non-interactively                                                                                                                                                                                     
recast tests run examples/higgsinos-hf test_statanalysis
```
After this you should have a new directory called `recast-test-*`.
The results of the run will be in `recast-test-*/HFoutput/HFresults/`. There you will find the output of the HF step, in this case the upperlimits for 2 signal points.
If something went wrong during the run, or you want to check the output, you can take a look at the log files in `recast-test-*/_packtivity/`.

Interactive runs are also possible (for debugging, testing, etc), to do so,
```
# to run in the shell (interactively)
$(recast tests shell examples/higgsinos-hf test_statanalysis)
# Now you will have to run all the command lines that you usually run (typically the ones you have in steps.yml)
source /recast_auth/getkrb.sh
source /home/atlas/release_setup.sh 
# and so on
```

## Running in git CI
To run the analysis in git (using git ci) you will have to fill the fields in 

Settings -> CI/CD -> Variables 

KRB_PASS # pwd 

RECAST_TOKEN # token 

KRB_USER # user

I changed the variable names RECAST_PASS (KRB_PASS) and RECAST_USER (KRB_USER) from above for testing reasons. Just make sure the variable names are consistent with the ones in the [gitlab-ci](./.gitlab-ci.yml) file.



# Histfitter
HistFitter package:
------------------------
Last update: 2018/04/11 <sophio.pataraia@cern.ch>

Recommended version:
We recommend to check out v0.61.0

Main documentation:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SusyFitter 

Tutorial wiki:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HistFitterTutorial

Mailing-list, for *any* of your questions:
HistFitter Group <atlas-phys-susy-histfitter@cern.ch>

When committing, don't forget to update ChangeLog before tagging!

Setup:
------
source setup.sh     (from lxplus, using cvmfs)
source setup_afs.sh    (from lxplus, using afs)

Recommended Root version:
-------------------------
The setup files automatically point at the recommended ROOT version to use. These reside on afs. 
Have a look at the setup script if you would like ot modify the ROOT version.
To install, follow the instructions in README/INSTALL.


Compilation:
------------
cd src
make
cd ..


Running the examples:
---------------------
See the tutorial:
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HistFitterTutorial

Directory structure:
--------------------
analysis ## contains all files related to an analysis. E.g. ZeroLepton/
data     ## contains data text files, provided externally, used to create workspaces for analysis
doc      ## documentation
examples ## example scripts that use the libraries in python/
lib	     ## location shared libary
macros	 ## macros for making plots, testing the fit, ongoing work, etc
python	 ## pyton base classes
results  ## where root files with workspaces generated by HistFactory get stored
scripts	 ## scripts for making workspaces based on text files in data/ . Scripts for submitting batch jobs.
src	     ## source code to make workspaces, do toys.


