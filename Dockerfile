#FROM rootproject/root-ubuntu16:6.10
#USER root
#COPY . /HistFitter
#RUN chown -R builder /HistFitter
#USER builder
#RUN bash -c "cd /HistFitter && \
#    . /usr/local/bin/thisroot.sh && \
#    . setup.sh && cd src && make"

#---------------------------------------
FROM atlas/analysisbase:21.2.71
USER atlas
ADD . /HistFitter/
WORKDIR /HistFitter/
RUN bash -c " sudo chown -R atlas /HistFitter && \
    cd /HistFitter && \
    source ~/release_setup.sh &&  \
    #git checkout tags/v0.63.0 -b tag_v0.63.0  && \
    #git checkout 15ab96defaa940cb16551262eaec971eda5f4ef1  && \
    cd ./src  && \
    #make -Bj && \
    make && \
    #cd ../../ "
    cd ../ "
    #git apply patches/patch_HistFitter.patch "
    #git apply patches/patch_HistFitter_Syst.patch "

