"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Simple example configuration with input trees                             *
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################


from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from optparse import OptionParser
import ROOT
from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
gROOT.LoadMacro("AtlasStyle/AtlasStyle.C")
ROOT.SetAtlasStyle()

myUserArgs = configMgr.userArg.split(' ')

myInputParser = OptionParser()
myInputParser.add_option('', '--SR', dest = 'SR', default = 'GRID')
#myInputParser.add_option("--doSignalPlots", action='store_true', default=False, help="do signal plots")
myInputParser.add_option("--doSignalPlots", action='store_true', default=True, help="do signal plots")
myInputParser.add_option("-s", "--split", dest='split', default='185_150', help="Mass splitting")
(options, args) = myInputParser.parse_args(myUserArgs)
whichSR = options.SR
doSignalPlots = options.doSignalPlots
split = options.split

#---------------------------------------------------------------------------------------------
# Some flags for overridding normal execution and telling ROOT to shut up... use with caution!
#---------------------------------------------------------------------------------------------
#gROOT.ProcessLine("gErrorIgnoreLevel=10001;")
#configMgr.plotHistos = True

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat=False
doValidation=False #use or use not validation regions to check exptrapolation to signal regions
what_to_plot = ["SR"]

#-------------------------------
# Parameters for hypothesis test
#-------------------------------
#configMgr.doHypoTest=False
configMgr.nTOYs=5000
configMgr.calculatorType=2
configMgr.testStatType=3
configMgr.nPoints=20

#--------------------------------
# Now we start to build the model
#--------------------------------

# First define HistFactory attributes
configMgr.analysisName = "MyEwkUpgrade_Grid"+whichSR
if doSignalPlots:
    configMgr.analysisName = "Signal_Grid"+whichSR

#activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = True #enable the fallback to trees
configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1. # Luminosity of input TTree after weighting
configMgr.outputLumi = 3000000. # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")
configMgr.blindSR=True
configMgr.blindCR=False
configMgr.useSignalInBlindedData=False #False per UpperLimit or exclusion

configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

if whichSR == "Grid":
    configMgr.histBackupCacheFile = "data/"+configMgr.analysisName+".root" 

configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"
#configMgr.outputFileName = "results/"+configMgr.analysisName+".root"

# Set the files to read from

ntupFiles = []
if configMgr.readFromTree:
    ntupFiles.append("samples/Bkg.root")
#    ntupFiles.append("root://eosuser.cern.ch///eos/user/s/susyupg/HFsamples/Bkg.root")
else:
    ntupFiles = [configMgr.histCacheFile]
    pass


# Dictionnary of cuts for Tree->hist
configMgr.cutsDict["CR"] = "NLEP < 0" # Unphysical CR to fool histfitter into making our plots!

#Low Mass Mll binning

configMgr.cutsDict["SR1"] = "NLEP == 2 && nbjet == 0 && isSF == 1 && isOS == 1 && dRll < 2. && MET > 500 && Mll < 3 && Mll > 1. && dPhiJ1MET > 2.0 && pt_jet1 > 100 && minDPhiAllJetsMet > 0.4 && METoverHT10 == 1 && (Mtt < 0 || Mtt > 160)"
configMgr.cutsDict["SR2"] = "NLEP == 2 && nbjet == 0 && isSF == 1 && isOS == 1 && dRll < 2. && MET > 500 && Mll < 5 && Mll > 3.2 && dPhiJ1MET > 2.0 && pt_jet1 > 100 && minDPhiAllJetsMet > 0.4 && METoverHT10 == 1 && (Mtt < 0 || Mtt > 160)"
configMgr.cutsDict["SR3"] = "NLEP == 2 && nbjet == 0 && isSF == 1 && isOS == 1 && dRll < 2. && MET > 500 && Mll < 10 && Mll > 5 && dPhiJ1MET > 2.0 && pt_jet1 > 100 && minDPhiAllJetsMet > 0.4 && METoverHT10 == 1 && (Mtt < 0 || Mtt > 160)"
configMgr.cutsDict["SR4"] = "NLEP == 2 && nbjet == 0 && isSF == 1 && isOS == 1 && dRll < 2. && MET > 500 && Mll < 20 && Mll > 10 && dPhiJ1MET > 2.0 && pt_jet1 > 100 && minDPhiAllJetsMet > 0.4 && METoverHT10 == 1 && (Mtt < 0 || Mtt > 160)"
configMgr.cutsDict["SR5"] = "NLEP == 2 && nbjet == 0 && isSF == 1 && isOS == 1 && dRll < 2. && MET > 500 && Mll < 30 && Mll > 20 && dPhiJ1MET > 2.0 && pt_jet1 > 100 && minDPhiAllJetsMet > 0.4 && METoverHT5 == 1 && (Mtt < 0 || Mtt > 160) && pt_lep2 > 8."
configMgr.cutsDict["SR6"] = "NLEP == 2 && nbjet == 0 && isSF == 1 && isOS == 1 && dRll < 2. && MET > 500 && Mll < 50 && Mll > 30 && dPhiJ1MET > 2.0 && pt_jet1 > 100 && minDPhiAllJetsMet > 0.4 && METoverHT5 == 1 && (Mtt < 0 || Mtt > 160) && pt_lep2 > 8."


# Tuples of nominal weights without and with b-jet selection
configMgr.weights = ("totalweight","1.")

#--------------------
# List of systematics
#--------------------

# SYST error
UserSys = Systematic("UserSys","",0.7,1.3,"user","userOverallSys")
SignalSys = Systematic("SignalSys","",0.85,1.15,"user","userOverallSys")


#BinSyst = Systematic("BinSyst","",0.8,1.2,"tree","histoSys")
BinSyst1 = Systematic("BinSyst1","",0.8,1.2,"user","userOverallSys")
BinSyst2 = Systematic("BinSyst2","",0.8,1.2,"user","userOverallSys")
BinSyst3 = Systematic("BinSyst3","",0.8,1.2,"user","userOverallSys")
BinSyst4 = Systematic("BinSyst4","",0.8,1.2,"user","userOverallSys")
BinSyst5 = Systematic("BinSyst5","",0.8,1.2,"user","userOverallSys")
BinSyst6 = Systematic("BinSyst6","",0.8,1.2,"user","userOverallSys")


# name of nominal histogram for systematics
configMgr.nomName = ""

# List of samples and their plotting colours
ttbar = Sample("ttbar_ntuple",kYellow-8)
ttbar.addInputs(ntupFiles)
ttbar.setStatConfig(useStat)
#ttbar.setNormFactor("mu_top",1.,0.,5.)
#ttbar.setNormRegions([("CR","cuts")])
ttbar.setNormByTheory()

Z_Jets = Sample("Z_Jets_ntuple", kBlue+3);
Z_Jets.addInputs(ntupFiles)
Z_Jets.setStatConfig(useStat)
Z_Jets.setNormByTheory()

W_Jets = Sample("W_Jets_ntuple", kAzure-4);
W_Jets.addInputs(ntupFiles)
W_Jets.setStatConfig(useStat)
W_Jets.setNormByTheory()

Diboson = Sample("VV_Jets_ntuple", kGray+1);
Diboson.addInputs(ntupFiles)
Diboson.setStatConfig(useStat)
Diboson.setNormByTheory()

data = Sample("Data",kBlack)
data.setData()

    
#Binnings

srNBins   = 1
srBinLow  = 0.5
srBinHigh = 1.5

#************
#Exclusion fit
#************


bkt = configMgr.addFitConfig("Exclusion")
if useStat:
    bkt.statErrThreshold=0.05 
else:
    bkt.statErrThreshold=None
   
bkt.addSamples([ttbar, Z_Jets, W_Jets, Diboson, data])



bkt.getSample("ttbar_ntuple").addSystematic(UserSys)
bkt.getSample("W_Jets_ntuple").addSystematic(UserSys)
bkt.getSample("Z_Jets_ntuple").addSystematic(UserSys)
bkt.getSample("VV_Jets_ntuple").addSystematic(UserSys)






sigSamples=[options.split]
#sigSamples=["107_100","110_100","120_100","135_100", "185_150"]

for sig in sigSamples:
    if doSignalPlots:
        #myTopLvl = configMgr.addFitConfigClone(bkt,"SimpleChannel_%s"%sig)
        sigSample = Sample("Signal_ntuple_"+sig,kPink)
        #sigSample.addInput("root://eosuser.cern.ch///eos/user/s/susyupg/HFsamples/Signal_grid_Extended.root")
        sigSample.addInput("samples/Signal_grid_Extended.root")
        #sigSample.setNormByTheory()
        sigSample.setStatConfig(useStat)
        sigSample.setNormFactor("mu_SIG",1.,0.,5.)
        sigSample.addSystematic(SignalSys)
        bkt.addSamples(sigSample)
        bkt.setSignalSample(sigSample)



### With METoverHT cut###
# For background estimation we used exponential fits in the MET tails to obtain the yields
# Here are the results of the fits
W_Jets.buildHisto([0.08],"SR1","cuts",0.5,1)
ttbar.buildHisto([1.68],"SR1","cuts",0.5,1)
Diboson.buildHisto([0.05],"SR1","cuts",0.5,1)
Z_Jets.buildHisto([0.68],"SR1","cuts",0.5,1)

W_Jets.buildHisto([0.89],"SR2","cuts",0.5,1)
ttbar.buildHisto([1.22],"SR2","cuts",0.5,1)
Diboson.buildHisto([0.82],"SR2","cuts" ,0.5,1)
Z_Jets.buildHisto([13.17],"SR2","cuts",0.5,1)

W_Jets.buildHisto([8.89],"SR3","cuts",0.5,1)
ttbar.buildHisto([7.53],"SR3","cuts",0.5,1)
Diboson.buildHisto([15.42],"SR3","cuts" ,0.5,1)
Z_Jets.buildHisto([30.65],"SR3","cuts",0.5,1)

W_Jets.buildHisto([25.81],"SR4","cuts" ,0.5,1)
ttbar.buildHisto([29.33],"SR4","cuts" ,0.5,1)
Diboson.buildHisto([40.68],"SR4","cuts" ,0.5,1)
Z_Jets.buildHisto([47.65],"SR4","cuts",0.5,1)

W_Jets.buildHisto([4.72],"SR5","cuts" ,0.5,1)
ttbar.buildHisto([22.02],"SR5","cuts" ,0.5,1)
Diboson.buildHisto([50.71],"SR5","cuts" ,0.5,1)
Z_Jets.buildHisto([26.18],"SR5","cuts",0.5,1)

#W_Jets.buildHisto([],"SR6","cuts" ,0.5,1) #bad exponential fit
ttbar.buildHisto([54.25],"SR6","cuts" ,0.5,1)
Diboson.buildHisto([105.61],"SR6","cuts" ,0.5,1)
Z_Jets.buildHisto([1.80],"SR6","cuts",0.5,1)

#data.buildHisto([1.],"CR","cuts",0.5,1)

meas=bkt.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.05)
meas.addPOI("mu_SIG")
meas.addParamSetting("Lumi",True,1)

#-------------------------------------------------
# Constraining regions - statistically independent
#-------------------------------------------------

#CRhist = bkt.addChannel("cuts",["CR"],1,0.5,1.5)
#CRhist.hasB = True   # no selection cuts implying b-tagging 
#CRhist.hasBQCD = False
#CRhist.useOverflowBin = False  
#bkt.addBkgConstrainChannels([CRhist])

#Single bin fit
#SRhist = bkt.addChannel("cuts",["SR"],1,0.5,1.5)

#For the multibin fit
#SRhist = bkt.addChannel("Mll",["SRM"],5,0.0,50.0)
#Multibin Fit for Simone
#SRhist = bkt.addChannel("Mll",["SRM"],6,0.0,60.0)


#Multibin sanity check
#SRhist = bkt.addChannel("cuts",["SRM"],1,0.5,1.5)
SRhist1 = bkt.addChannel("cuts",["SR1"],1,0.5,1.5)
SRhist2 = bkt.addChannel("cuts",["SR2"],1,0.5,1.5)
SRhist3 = bkt.addChannel("cuts",["SR3"],1,0.5,1.5)
SRhist4 = bkt.addChannel("cuts",["SR4"],1,0.5,1.5)
SRhist5 = bkt.addChannel("cuts",["SR5"],1,0.5,1.5)
SRhist6 = bkt.addChannel("cuts",["SR6"],1,0.5,1.5)

#SRhist.hasB = True   # no selection cuts implying b-tagging 
#SRhist.hasBQCD = False
#SRhist.useOverflowBin = False

#For the Single bin and Multibin
#bkt.addSignalChannels([SRhist])


#For the Multibin fit
bkt.addSignalChannels([SRhist1,SRhist2,SRhist3,SRhist4,SRhist5,SRhist6])




SRhist1.hasB = True   # no selection cuts implying b-tagging 
SRhist1.hasBQCD = False
SRhist1.useOverflowBin = False
SRhist1.addSystematic(BinSyst1)

SRhist2.hasB = True   # no selection cuts implying b-tagging 
SRhist2.hasBQCD = False
SRhist2.useOverflowBin = False
SRhist2.addSystematic(BinSyst2)

SRhist3.hasB = True   # no selection cuts implying b-tagging 
SRhist3.hasBQCD = False
SRhist3.useOverflowBin = False
SRhist3.addSystematic(BinSyst3)

SRhist4.hasB = True   # no selection cuts implying b-tagging 
SRhist4.hasBQCD = False
SRhist4.useOverflowBin = False
SRhist4.addSystematic(BinSyst4)

SRhist5.hasB = True   # no selection cuts implying b-tagging 
SRhist5.hasBQCD = False
SRhist5.useOverflowBin = False
SRhist5.addSystematic(BinSyst5)

SRhist6.hasB = True   # no selection cuts implying b-tagging 
SRhist6.hasBQCD = False
SRhist6.useOverflowBin = False
SRhist6.addSystematic(BinSyst6)



